using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;

    //public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement; 

    public bool onInteractionZone {  get; set; }
    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        //Get input from player
        input.getInput();

        //Move the character
        characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash); 
    }
}
